package info.renjithv.jira.plugins.sysadmin.rest;

import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static info.renjithv.jira.plugins.sysadmin.rest.PermUtility.isSysAdmin;

/**
 * A resource of message.
 */
@Path("/console")
@WebSudoRequired
public class DbConsoleRest {
    private static final Logger log = LogManager.getLogger(DbConsoleRest.class);

    @GET
    @AnonymousAllowed
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("execute")
    public Response executeSql(@QueryParam("sql") String sql) throws SQLException {
        if(!isSysAdmin())
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        DbConsoleRestModel resp = new DbConsoleRestModel();
        if (null != sql && !sql.isEmpty()) {
            Connection connection = new DefaultOfBizConnectionFactory().getConnection();
            try {
                log.debug("SQL - " + sql);
                resp.setSql(sql);

                Statement stmt = connection.createStatement();
                if(stmt.execute(sql))
                {
                    resp.setResultType(1);
                    final ResultSet resultSet = stmt.getResultSet();

                    ResultSetMetaData rsm = resultSet.getMetaData();
                    int columnCount = rsm.getColumnCount();

                    //Add column names
                    for (int nCol = 1; nCol <= columnCount; nCol++) {
                        resp.addColumn(rsm.getColumnName(nCol));
                    }

                    //Add data rows
                    while (resultSet.next()) {
                        List<String> row = new ArrayList<String>();
                        for (int nCol = 1; nCol <= columnCount; nCol++) {
                            row.add(String.valueOf(resultSet.getObject(nCol)));
                        }
                        resp.addRow(row);
                    }

                    resultSet.close();
                }
                else
                {
                    resp.setResultType(0);
                    resp.setUpdatedCount(stmt.getUpdateCount());
                }

                return Response.ok(resp).build();
            } catch (Exception e) {
                resp.setError(e.getMessage());
                return Response.ok(resp).build();
            }
            finally {
                connection.close();
            }
        }
        else
        {
            resp.setError("Bad request, probably sql parameter missing");
            return Response.ok(resp).build();
        }
    }
}