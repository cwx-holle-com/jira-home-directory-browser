package info.renjithv.jira.plugins.sysadmin.jira.webwork;

import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

@WebSudoRequired
public class DbConsoleModuleAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(DbConsoleModuleAction.class);

    @Override
    public String execute() throws Exception {

        return super.execute(); //returns SUCCESS
    }
}
