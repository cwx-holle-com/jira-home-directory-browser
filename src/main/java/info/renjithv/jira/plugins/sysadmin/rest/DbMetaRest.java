package info.renjithv.jira.plugins.sysadmin.rest;

import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static info.renjithv.jira.plugins.sysadmin.rest.PermUtility.isSysAdmin;

/**
 * A resource of message.
 */
@Path("/meta")
@WebSudoRequired
public class DbMetaRest {
    private static final Logger log = LogManager.getLogger(DbMetaRest.class);
    private static List<String> ignoreList = new ArrayList<String>();

    public DbMetaRest() {
        ignoreList.add("SYSTEM_");
    }

    @GET

    @AnonymousAllowed
    @Path("data")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage() throws SQLException {
        if(!isSysAdmin())
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        Connection c = new DefaultOfBizConnectionFactory().getConnection();
        List<DbTable> tables = new ArrayList<DbTable>();
        try {
            DatabaseMetaData md = c.getMetaData();
            ResultSet rs = md.getTables(null, null, "%", null);
            while (rs.next()) {
                final String catalog = rs.getString(1);
                final String schema = rs.getString(2);
                final String name = rs.getString(3);
                final String type = rs.getString(4);
                if(!type.equals("TABLE")){
                    continue;
                }
                boolean ignoreTable = false;
                for(String ignore: ignoreList){
                    if(name.startsWith(ignore)){
                        ignoreTable = true;
                        break;
                    }
                }
                if(ignoreTable){
                    continue;
                }
                final ResultSet columns = md.getColumns(catalog, schema, name, null);

                DbTable table = new DbTable(name);
                while(columns.next()){
                    table.addColumn(
                            columns.getString(4),
                            columns.getString(4),
                            columns.getString(5),
                            columns.getInt(7)
                    );
                }
                tables.add(table);
            }
        }
        catch (Exception e){
            log.error("Database error - " + e);
        }
        finally {
            c.close();
        }
        return Response.ok(tables).build();
    }
}