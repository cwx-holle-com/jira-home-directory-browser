package info.renjithv.jira.plugins.sysadmin.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class DbConsoleRestModel {

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    @XmlElement(name = "resultType")
    private int resultType;

    public int getUpdatedCount() {
        return updatedCount;
    }

    public void setUpdatedCount(int updatedCount) {
        this.updatedCount = updatedCount;
    }

    @XmlElement(name="updatedCount")
    private int updatedCount;

    @XmlElement(name = "error")
    private String error;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    @XmlElement(name = "sql")
    private String sql;

    @XmlElement(name = "columns")
    private List<String> columns = new ArrayList<String>();

    @XmlElement(name = "rows")
    private List<List<String>> rows = new ArrayList<List<String>>();

    public DbConsoleRestModel() {
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public void setRows(List<List<String>> rows) {
        this.rows = rows;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }
    public void addColumn(String column){
        columns.add(column);
    }
    public void addRow(List<String> row){
        rows.add(row);
    }

    public void setError(String error) {
        this.error = error;
    }
}