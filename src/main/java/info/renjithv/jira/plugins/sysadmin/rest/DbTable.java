package info.renjithv.jira.plugins.sysadmin.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "")
@XmlAccessorType(XmlAccessType.FIELD)

public class DbTable {
    @XmlElement(name = "name")
    public String name;

    @XmlElement(name = "columns")
    public List<Column> columns = new ArrayList<Column>();

    public DbTable(String n) {
        name = n;
    }
    public void addColumn(Column col){
        columns.add(col);
    }

    public void addColumn(String name, String label, String type, int size) {
        columns.add(new Column(name, label, type, size));
    }

    public class Column{
        @XmlElement(name = "name")
        public String name;

        @XmlElement(name = "label")
        public String label;

        @XmlElement(name = "type")
        public String type;

        @XmlElement(name = "size")
        public int size;

        public Column(String name, String label, String type, int size) {
            this.name = name;
            this.label = label;
            this.type = type;
            this.size = size;
        }
    }
}
