package info.renjithv.jira.plugins.sysadmin.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.Permissions;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */
public class PermUtility {
    public static boolean isSysAdmin(){
        if( !ComponentAccessor.getPermissionManager().hasPermission(Permissions.SYSTEM_ADMIN,
                ComponentAccessor.getJiraAuthenticationContext().getUser()) )
        {
            return false;
        }
        else {
            return true;
        }
    }
}
