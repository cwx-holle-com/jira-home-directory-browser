package info.renjithv.jira.plugins.sysadmin.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlRootElement(name = "logdata")
@XmlAccessorType(XmlAccessType.FIELD)
public class LogDataModel {
    @XmlElement(name = "id")
    private String id;

    @XmlElement(name = "data")
    private String data = "";

    public String getLastUpdated() {
        return lastUpdated;
    }

    @XmlElement(name= "lastupdated")
    String lastUpdated = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss").format(new Date());

    public LogDataModel(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void addData(String data) {
        this.data += data + "\n";
    }
}