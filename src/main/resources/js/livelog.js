AJS.$(function ($) {
    function close(id) {
        $.getJSON(AJS.contextPath() + "/rest/livelogrest/latest/livelog/close?id=" + id)
            .success(function (data) {
                console.log("Closed");
            });
    }
    function getId(editor) {
        var timerId;
        $.getJSON(AJS.contextPath() + "/rest/livelogrest/latest/livelog/id?filename=" + $('#filename').val())
            .success(function (data) {
                //console.log(data);
                var id = data.id;
                timerId = setInterval(function () {
                    $.getJSON(AJS.contextPath() + "/rest/livelogrest/latest/livelog/data?filename=" +
                        $('#filename').val() + "&id=" + id,
                        function (data) {
                            if (data.data && data.data != null) {
                                editor.setValue(editor.getValue() + data.data);
                                // window.scroll(0,document.height);
                                var pageHeight = getPageHeight();
                                if (scrollOnUpdate()) {
                                    window.scroll(0, pageHeight);
                                }
                                $('#lastupdated').text("Last updated: " + data.lastupdated);
                            }
                        }
                    );
                }, 2000);

                function getPageHeight() {
                    var body = document.body;
                    var html = document.documentElement;

                    var height = Math.max(body.scrollHeight, body.offsetHeight,
                        html.clientHeight, html.scrollHeight, html.offsetHeight);
                    return height;
                }

                function scrollOnUpdate() {
                    return $('#scroll-btn').is('[aria-pressed]');
                }

                $(window).unload(function () {
                    close(data.id);
                });
            });
        return timerId;
    }

    var editor = CodeMirror.fromTextArea(document.getElementById("logview"), {
        mode: 'text/x-erlang',
        readOnly: "nocursor",
        lineWrapping: true
    });
    var timer = getId(editor);

    $('#back').click(function(event){
        event.preventDefault();
        history.back();
    });

    var $scrollOnUpdateButtons = $('#scroll-on-update').find('button');
    $scrollOnUpdateButtons.click(function() {
        $scrollOnUpdateButtons.removeAttr('aria-pressed');
        $(this).attr('aria-pressed', true);
    });
});
